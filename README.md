
# Vagrant Manager - Shell

## Description

Vagrant Manager Shell is an application built on Ruby to help manage Vagrant-based virtual machines.

## Install & Run
This version only requires vm.rb file, download and execute using ruby: 

```
$ ruby vm.rb
```

## Features

* Manage your VMs lifecycle : create, start, stop, suspend, bounce, destroy, provision.
![ControlMenu](vm-shell-images/vm-shell-controlmenu.jpg)
![ControlMenu](vm-shell-images/vm-shell-mgtmenu.jpg)
![ControlMenu](vm-shell-images/vm-shell-newvm.jpg)
* Add or increase or reduce resources to/from your VMs : disk, cpu, memory.
* Get information about your VMs : Network IPs, OS, boxes, providers.
![Detailed Info](vm-shell-images/vm-shell-getdetails.jpg)
* Save your configuration settings for easy retrieval and better administration of your VMs.
* Navigate your VMs using your arrow keys.
![Navigation](vm-shell-images/vm-shell-nav1.jpg)
![Navigation Right Arrow](vm-shell-images/vm-shell-nav2.jpg)
* Color-coded to easy find those VMs that are in running, pause or halt status.


## TO DO Features

* Add command line arguments for quick access to features.
* Add VMWare support.
* Add Docker support.
