require 'io/console'
require 'fileutils'
require 'json'

def startvm vmname
    if ! $vm_state["#{vmname}"].empty?
        vmid=$vm_state["#{vmname}"][0]
        vmdir=$vm_state["#{vmname}"][1]
        
        if Dir.exist? ("#{vmdir}")
            system("vagrant up #{vmid}")
        else
            puts "Please verify container directory is correct : #{vmdir}"
        end
    end
end

def stopvm vmname
    if ! $vm_state["#{vmname}"].empty?
        vmid=$vm_state["#{vmname}"][0]
        vmdir=$vm_state["#{vmname}"][1]
        
        if Dir.exist? ("#{vmdir}")
            system("vagrant halt #{vmid}")
        else
            puts "Please verify container directory is correct : #{vmdir}"
        end
    end
end

def deletevm vmname
    puts "This is a destructive and irreparable action"
    puts "the VM and all its files will be removed from the system"
    puts "Enter the name of the VM to confirm :"
    del_input = gets.chomp
    
    if del_input != vmname
        puts "the names differ, skipping VM deletion..."
        return false
    else
        puts "very well, destroying VM..."
    end

    if ! $vm_state["#{vmname}"].empty?
        vmid=$vm_state["#{vmname}"][0]
        vmdir=$vm_state["#{vmname}"][1]
        
        if Dir.exist? ("#{vmdir}")
            system("vagrant destroy -f #{vmid}")
        else
            puts "Please verify container directory is correct : #{vmdir}"
        end
    end

    FileUtils.remove_dir("#{vmdir}")
    get_vm_state
end

def suspendvm vmname
    if ! $vm_state["#{vmname}"].empty?
        vmid=$vm_state["#{vmname}"][0]
        vmdir=$vm_state["#{vmname}"][1]
        
        if Dir.exist? ("#{vmdir}")
            system("vagrant suspend #{vmid}")
        else
            puts "Please verify container directory is correct : #{vmdir}"
        end
    end
end

def resumevm vmname
    if ! $vm_state["#{vmname}"].empty?
        vmid=$vm_state["#{vmname}"][0]
        vmdir=$vm_state["#{vmname}"][1]
        
        if Dir.exist? ("#{vmdir}")
            system("vagrant resume #{vmid}")
        else
            puts "Please verify container directory is correct : #{vmdir}"
        end
    end
end

def restartvm vmname,force=false
    if ! $vm_state["#{vmname}"].empty?
        vmid=$vm_state["#{vmname}"][0]
        vmdir=$vm_state["#{vmname}"][1]
        if Dir.exist? ("#{vmdir}")
            if force
                system("vagrant reload #{vmid}")
            else
                puts "please confirm we can restart the VM [y|n]:"
                restart_input = gets.chomp
                if restart_input == "y"
                    system("vagrant reload #{vmid}")
                else
                    print "a server restart is needed for any changes to take effect\n"
                end
            end
        else 
            puts "Please verify container directory is correct : #{vmdir}"
        end
    end
end

def privisionvm vmname
    if ! $vm_state["#{vmname}"].empty?
        vmid=$vm_state["#{vmname}"][0]
        vmdir=$vm_state["#{vmname}"][1]
        
        if Dir.exist? ("#{vmdir}")
            system("vagrant provision #{vmid}")
        else
            puts "Please verify container directory is correct : #{vmdir}"
        end
    end
end

def vagrant_template vm_prop
    vtemplate = []
    if vm_prop.size != 0
        vtemplate[0] = "Vagrant.configure(\"2\") do |config|"
        vtemplate.push("  config.vm.define :#{vm_prop[0]} do |server|")
        vtemplate.push("    server.vm.box = \"#{vm_prop[1]}/#{vm_prop[3]}-#{vm_prop[4]}\"")
        vtemplate.push("    server.vm.network \"public_network\", bridge: \"em1\"")
        vtemplate.push("    server.vm.hostname = \"#{vm_prop[0]}\"")
        vtemplate.push("    server.vm.synced_folder \"#$containers_dir/shared\", \"/shared\"")
        vtemplate.push("    ")
        vtemplate.push("    server.vm.provider \"virtualbox\" do |vb|")
        vtemplate.push("      vb.customize [\"modifyvm\", :id, \"--name\", \"#{vm_prop[0]}\", \"--cpus\", \"#{vm_prop[6]}\", \"--memory\", #{vm_prop[5]},\"--ioapic\", \"on\"]")
        vtemplate.push("      vb.customize [\"storagectl\", :id, \"--name\", \"SATA Controller\", \"--controller\", \"IntelAHCI\", \"--hostiocache\", \"on\"]")
        vtemplate.push("    end")
        vtemplate.push("  # server.vm.provision :shell, path: \"provision-database\", args: ENV['ARGS']")
        vtemplate.push("    if File.exist? (\"#$containers_dir/shared/provision/scripts/provision_admin_user.sh\")")
        vtemplate.push("      server.vm.provision \"shell\", inline: <<-SHELL")
        vtemplate.push("        /shared/provision/scripts/provision_admin_user.sh")
        vtemplate.push("      SHELL")
        vtemplate.push("    end")
        vtemplate.push("  end")
        vtemplate.push("end")
    end

    vtemplate
end

def createvm
    vm_properties = []

    print "VM Name [#{RED}null#{NORMAL}]: "
    input = gets.downcase.chomp
    if input.empty? 
        print "Skipping due to empty or invalid VM Name"
        return true
    else 
        vm_properties[0] = input 
    end

    print "Vagrant Box [#{BLUE}bento#{NORMAL} ubuntu] : " 
    input = gets.downcase.chomp
    if input.empty? 
        vm_properties[1] = "bento"
    else 
        vm_properties[1] = input 
    end

    print "VM Provider [#{BLUE}VirtualBox#{NORMAL} VMWare] : " 
    input = gets.downcase.chomp
    if input.empty? 
        vm_properties[2] = "virtualbox"
    else 
        vm_properties[2] = input 
    end

    print "Guest OS Name [#{BLUE}oracle#{NORMAL} centos ubuntu redhat] : " 
    input = gets.downcase.chomp
    if input.empty? 
        vm_properties[3] = "oracle"
    else 
        vm_properties[3] = input 
    end
    
    print "OS Version [#{BLUE}7.6#{NORMAL} 7.7 8.0] : " 
    input = gets.chomp.to_f
    if input.zero? 
        vm_properties[4] = "7.6"
    else 
        vm_properties[4] = input 
    end

    print "Memory Size (#{BLUE}1024#{NORMAL}): "
    input = gets.chomp.to_i
    if input.zero? 
        vm_properties[5] = 1024
    else 
        vm_properties[5] = input 
    end

    print "CPU count (#{BLUE}1#{NORMAL}): "
    input = gets.chomp.to_i
    if input.zero? 
        vm_properties[6] = 1
    else 
        vm_properties[6] = input 
    end

    working_dir = Dir.pwd
    vagrantfile = vagrant_template vm_properties

    if Dir.exist? ("#$containers_dir/#{vm_properties[0]}")
        print "Virtual Machine already exists"
        false
    else
        Dir.chdir "#$containers_dir"
        Dir.mkdir (File.join(Dir.pwd, "#{vm_properties[0]}"))
        Dir.chdir "#$containers_dir/#{vm_properties[0]}"
        File.open("Vagrantfile","w+") do |v|
            for line in 0...vagrantfile.size do
                v.write("#{vagrantfile[line]}\n")
            end
        end

        system("vagrant up")
        vm_properties[7]   = [`vagrant ssh -c \"ifconfig eth0\"  | grep -w inet | awk '{ print $2 }'`.chomp]
        vm_properties[7].push(`vagrant ssh -c \"ifconfig eth1\"  | grep -w inet | awk '{ print $2 }'`.chomp)

        File.open("#{vm_properties[0]}.vm", "w+") do |f| 
            f.write("{\n") 
            f.write("  \"vm_name\": \"#{vm_properties[0]}\",\n") 
            f.write("  \"vagrant_box\": \"#{vm_properties[1]}\",\n") 
            f.write("  \"vm_provider\": \"#{vm_properties[2]}\",\n") 
            f.write("  \"os_name\": \"#{vm_properties[3]}\",\n") 
            f.write("  \"os_version\": \"#{vm_properties[4]}\",\n") 
            f.write("  \"mem_size\": \"#{vm_properties[5]}\",\n") 
            f.write("  \"cpu_count\": \"#{vm_properties[6]}\",\n") 
            f.write("  \"network\": {\n")
            f.write("      \"eth0\": \"#{vm_properties[7][0]}\",\n")
            f.write("      \"eth1\": \"#{vm_properties[7][1]}\"\n")
            f.write("   }\n") 
            f.write("}\n")
        end

        Dir.chdir "#{working_dir}" 
        get_vm_state
        true
    end
end

def add_disk vmname, set_size
    working_dir =  Dir.pwd
    Dir.chdir "#$vb_home/#{vmname}"
    disks = Dir.glob("*.vmdk")
    next_port = disks.size+1.to_i
    next_disk = "disk#{next_port}"
    exit_code = `VBoxManage createmedium --filename \"#$vb_home/#{vmname}/#{next_disk}.vmdk\" --size #{set_size} --format vmdk`.to_i
    return false if exit_code != 0
    exit_code = `VBoxManage storageattach #{vmname} --storagectl \"SATA Controller\" --medium \"#$vb_home/#{vmname}/#{next_disk}.vmdk\" --port #{next_port} --type hdd`.to_i
    return false if exit_code != 0
    Dir.chdir "#{working_dir}"
    true
end

def add_uniform_disks vmname, counter
    print "\nEnter the size of the disk size (in MB): "
    set_size = gets.chomp.to_i
    if ! set_size.zero?
        while counter > 0
            add_disk(vmname, set_size)
            counter = counter-1
        end
    end
end

def add_nonuniform_disks vmname, counter
    disk_counter=1
    while counter > 0
        print "\nEnter the size of the disk ##{disk_counter} size (in MB): "
        set_size = gets.chomp.to_i
        if ! set_size.zero?
            add_disk(vmname, set_size)
            counter = counter-1
            disk_counter = disk_counter+1
        else
            print "\nYou didn't provide any value, you want to cancel(c)?"
            user_input = gets.downcase.chomp
            case user_input
            when "c"
                return false
            end
        end
    end
end

def adddiskvm vmname
    working_dir = Dir.pwd

    if ! Dir.exist? ("#$containers_dir/#{vmname}")
        print "Virtual Machine doesn't exists"
        false
    else
        Dir.chdir "#$containers_dir/#{vmname}"
        
        print "How many disks additional disks are needed? [1]: "
        num_disks = gets.chomp.to_i

        if num_disks.zero?
            num_disks = 1
        end

        if num_disks > 1
            print "Same size equally? [y|n] : "
            size_response = gets.downcase.chomp

            if size_response.empty?
                puts "I'll take that as a no then"
                return false
            end
        end

        puts "We need to bring the VM down before we can attach the new disks to the VM"
        puts "please confirm we can proceed with this [y|n]:"
        stop_input = gets.chomp
        
        if stop_input == "y"
            stopvm vmname
        else
            puts "very well, no action will be taken..."
            return
        end

        if num_disks == 1
            add_uniform_disks(vmname, 1)
        else
            add_uniform_disks(vmname, num_disks) if size_response == "y"
            add_nonuniform_disks(vmname, num_disks) if size_response == "n"
        end      
    end

    startvm vmname
    Dir.chdir "#{working_dir}" 
end

def rezcpuvm vmname
    if ! $vm_state["#{vmname}"].empty?
        vmid=$vm_state["#{vmname}"][0]
        vmdir=$vm_state["#{vmname}"][1]
        
        working_dir = Dir.pwd
        vm_properties = []

        if ! Dir.exist? ("#{vmdir}")
            print "Virtual Machine doesn't exists"
            false
        else
            Dir.chdir "#{vmdir}"
    
            print "How many CPUs are needed? [1]: "
            num_cpus = gets.chomp.to_i

            if num_cpus.zero?
                num_cpus = 1
            end

            vmprop = get_properties vmname
            vmprop["cpu_count"] = num_cpus
            vm_properties = json2array vmprop
            vagrantfile = vagrant_template vm_properties

            FileUtils.mv  "Vagrantfile", ".Vagrantfile.prev"
            File.open("Vagrantfile","w+") do |v|
                for line in 0...vagrantfile.size do
                    v.write("#{vagrantfile[line]}\n")
                end
            end

            FileUtils.mv  "#{vmname}.vm", ".#{vmname}.vm"
            f = File.open("#{vmname}.vm", "w+")
            f.write(vmprop.to_json)
            f.close

            restartvm vmname,false
        end

        Dir.chdir "#{working_dir}" 
    end
end

def rezmemvm vmname
    if ! $vm_state["#{vmname}"].empty?
        vmid=$vm_state["#{vmname}"][0]
        vmdir=$vm_state["#{vmname}"][1]
        
        working_dir = Dir.pwd
        vm_properties = []

        if ! Dir.exist? ("#{vmdir}")
            print "Virtual Machine doesn't exists"
            false
        else
            Dir.chdir "#{vmdir}"
        
            print "How much Memory is needed? [1024]: "
            mem_size = gets.chomp.to_i

            if mem_size.zero?
                mem_size = 1024
            end

            vmprop = get_properties vmname
            vmprop["mem_size"] = mem_size
            vm_properties = json2array vmprop
            vagrantfile = vagrant_template vm_properties

            FileUtils.mv  "Vagrantfile", ".Vagrantfile.prev"
            File.open("Vagrantfile","w+") do |v|
                for line in 0...vagrantfile.size do
                    v.write("#{vagrantfile[line]}\n")
                end
            end

            FileUtils.mv  "#{vmname}.vm", ".#{vmname}.vm"
            f = File.open("#{vmname}.vm", "w+")
            f.write(vmprop.to_json)
            f.close

            restartvm vmname,false
        end

        Dir.chdir "#{working_dir}" 
    end
end

def get_properties vmname
    
    get_vm_state

    begin
        if ! $vm_state["#{vmname}"].empty?
            vmid=$vm_state["#{vmname}"][0]
            vmdir=$vm_state["#{vmname}"][1]
            
            if Dir.exist? ("#{vmdir}")
                if File.exist? ("#{vmdir}/#{vmname}.vm")
                    f = File.open("#{vmdir}/#{vmname}.vm", "r")
                    data = JSON.load f
                    f.close
                end
            else
                puts "Please verify container directory is correct : #{vmdir}"
            end
        end

        data
    rescue
        puts "VM doesn't exist"
        data = nil
    end
end

def get_vm_status vmname
    working_dir = Dir.pwd
    Dir.chdir "#$containers_dir/#{vmname}"
    state = `vagrant status #{vmname} | grep -v grep | grep #{vmname} | awk '{print $2}'`.chomp
    Dir.chdir "#{working_dir}"
    state 
end

def get_vm_state
    $vm_state = {}
    vastatus = vstatus
    vastatus.each_with_index do | vmname, index|
        $vm_state[vmname[1]] = [vmname[0], vmname[4], vmname[3], vmname[2]] # [0] ID, [4] Directory, [3] State, [2] Provider
    end
end

def listrunning
    @list = []
    vbox = `vboxmanage list runningvms`
    vbox.split("\n").each_with_index do |vname, index|
        @list[index] = vname.split(" ")[0].gsub("\"","")
    end
    @list
end

def listvms
    @list = []
    vbox = `vboxmanage list vms`
    vbox.split("\n").each_with_index do |vname, index|
        @list[index] = vname.split(" ")[0].gsub("\"","")
    end
    @list
end

def vstatus
    @list = []
    counter = 0
    vbox = `vagrant global-status --prune`
    vbox.split("\n").each_with_index do |vname, index|
        next if index < 2
        @list[counter]     = [vname.split(" ")[0], vname.split(" ")[1], vname.split(" ")[2], vname.split(" ")[3], vname.split(" ")[4] ]
        counter += 1
    end

    #Remove last 7 lines of vagrant command
    for i in 1..7
        @list.pop()
    end

    @list
end

def get_user_input
    while user_input = $stdin.getch do
        begin
            while next_char = $stdin.read_nonblock(1) do
                user_input = "#{user_input}#{next_char}"
            end
        rescue IO::WaitReadable
        end
        
        parse_main_input user_input
    end
end

def get_vm_name
    user_input = gets.chomp
    #user_input.downcase!
end

def print_json jdata,ind=0
    indent=" "*ind
    jdata.keys.each do |k|
        case jdata[k].class.to_s   
        when "Integer"
            puts "#{indent}#{k}: #{jdata[k]}"
        when "Float"
            puts "#{indent}#{k}: #{jdata[k]}"
        when "String"
            puts "#{indent}#{k}: #{jdata[k]}"
        when "Hash"
            puts "#{indent}#{k}: "
            print_json jdata[k],ind+2
        end
    end
end

def json2array jdata,ignore_keys=true
    adata = []
    if ignore_keys
        jdata.keys.each_with_index do |key,index|
            adata[index] = jdata[key]
        end
    else
        jdata.keys.each_with_index do |key,index|
            adata[index] = [key, jdata[key]]
        end
    end 
    adata
end

def parse_main_input user_input
    clear_screen
    set_cursor 3,1
    case user_input
    when "c"
        controlSubMenu
    when "m"
        managementSubMenu
    when "s"
        stateSubMenu
    when "q"
        if ! $config_file_found
            print "Do you want to save current configuration? [y|n]: "
            save_input = gets.chomp
            case save_input
            when "y"
                vmprop = {}
                vmprop = {"vb_home" => "#$vb_home","containers_dir" => "#$containers_dir"}
                f = File.open("./vm.config","w+")
                f.write(vmprop.to_json)
                f.close
                puts("File ./vm.config created")
            else
                print "\n"
            end
        end
        exit
    else
        clear_screen 
        puts "Invalid or not implemented option"
    end
    mainMenu
end

def clear_screen
    print "\e[2J"
end

def get_key
    user_input = $stdin.getch
        begin
            while next_chars = $stdin.read_nonblock(10) do
                user_input = "#{user_input}#{next_chars}"
            end
        rescue IO::WaitReadable
        end

    user_input
end

def pop_screen vstatus,curposx,curposy
    while true
        print "\e[2J"
        screen=[]
        status=[]
        menu = []
        set_cursor 1, 1
        # VM Control
        menu[0] = "Enter for Control"
        # VM Management
        menu.push("Management (#{RED}m#{NORMAL})") #7
        # VM Status
        menu.push("VM State (#{RED}s#{NORMAL})") #7
        # Exit Program
        menu.push("Quit (#{RED}q#{NORMAL})") #8
        puts menu.join(' | ')

        collen=35   # Max String Length
        maxconposx = $stdin.winsize[0]
        maxconposy = $stdin.winsize[1]
        numcols = ( maxconposy / collen ).round
        
        if ( vstatus.size % numcols ) == 0
            numrows = ( vstatus.size / numcols )
        else
            numrows = ( vstatus.size / numcols ) + 1
        end

        #Initialize screen matrix
        for r in 0...numrows
            screen[r]=[]
            status[r]=[]
        end

        for r in 0...numrows
            for c in 0...numcols
                screen[r][c]=" "
                status[r][c]=" "
            end
        end

        #status = screen

        cols=0
        rows=0

        vstatus.each_with_index do |value, index|
            screen[rows][cols] = vstatus[index][1]
            status[rows][cols] = vstatus[index][3]

            if cols <= numcols  
                cols = cols + 1
                if cols == numcols  
                    cols = 0
                    rows = rows + 1
                    if rows == numrows
                        break
                    end
                end
            end
        end

        for r in 0...screen.size
            for c in 0...screen[r].size

                case status[r][c]
                when "running"
                    color=GREEN  
                when "saved"
                    color=YELLOW 
                when "poweroff"
                    color=RED    
                end
                
                cols=(c * collen)+1
                rows=r+3

                set_cursor rows, cols

                if curposx == r && curposy == c
                    printf "#{color}%4s %-30s","=>", screen[r][c]
                else
                    printf "#{color}%4s %-30s"," " , screen[r][c]
                end
            end
        end

        print "#{NORMAL}"
        set_cursor $stdin.winsize[0],1
        #set_cursor $stdin.winsize[0]-2,1
        #print "(#{curposx},#{curposy}) Matrix Size: (#{r+1},#{c+1}) numrows: #{numrows} numcols: #{numcols} index: #{idx} array size: #{vstatus.size}"
        key=get_key
        case key
        when "\n","\r"
            begin
                controlSubMenu screen[curposx][curposy]
            rescue
            end
        when "\e[A" #Up
            curposx = 1 if curposx < 1
            pop_screen vstatus,curposx-1,curposy
        when "\e[B" #Down
            curposx = curposx - 1 if curposx + 1 == screen.size 
            curposx = curposx - 1 if screen[curposx+1][curposy] == " "
            pop_screen vstatus,curposx+1,curposy
        when "\e[C" #Right
            curposy = curposy - 1 if curposy + 1 == screen[curposx].size
            curposy = curposy - 1 if screen[curposx][curposy+1] == " "
            pop_screen vstatus,curposx,curposy+1
        when "\e[D" #Left
            curposy = 1 if curposy < 1
            pop_screen vstatus,curposx,curposy-1
        when "m"
            clear_screen
            managementSubMenu
        when "s"
            clear_screen
            stateSubMenu
        when "q"
            exit
        end

        mainMenu
    end
end

def mainMenu
    print "\e[?25l"
    vastatus = vstatus
    pop_screen vastatus,0,0
end

#Control 
def get_control_input vmname
    while user_input = $stdin.getch do
        begin
            while next_char = $stdin.read_nonblock(1) do
                user_input = "#{user_input}#{next_char}"
            end
        rescue IO::WaitReadable
        end
        
        parse_control_input user_input,vmname
    end
end

def parse_control_input user_input,vmname
    clear_screen
    set_cursor 3,1
    case user_input
    when "s"
        startvm vmname
    when "x"
        stopvm vmname
    when "e"
        suspendvm vmname
    when "m"
        resumevm vmname
    when "o"
        restartvm vmname
    when "p"
        privisionvm vmname
    when "u"
        clear_screen
        updateSubMenu vmname
    when "d"
        vm = get_properties vmname
        print_json vm if ! vm.nil?
        print "\nPress any key to continue..."
        user_input = gets.chomp
    when "c"
        mainMenu
    else
        clear_screen 
        puts "Invalid or not implemented option"
    end
    controlSubMenu vmname
end

def controlSubMenu vmname
    actions = []
    clear_screen
    set_cursor 1,1
    actions[0] = "#{BLUE}VM: #{vmname} - #{NORMAL} Start (#{RED}s#{NORMAL})" #9
    actions.push("Stop (#{RED}x#{NORMAL})") #8
    actions.push("Save (#{RED}e#{NORMAL})") #14
    actions.push("Resume (#{RED}m#{NORMAL})") #10
    actions.push("Reload (#{RED}o#{NORMAL})") #10
    actions.push("Provision (#{RED}p#{NORMAL})") #13
    actions.push("Update (#{RED}u#{NORMAL})") #13
    actions.push("Details (#{RED}d#{NORMAL})") #13
    actions.push("Close Menu (#{RED}c#{NORMAL})") #14
    puts actions.join(' | ')
    get_control_input vmname 
end

#Management 
def get_management_input
    while user_input = $stdin.getch do
        begin
            while next_char = $stdin.read_nonblock(1) do
                user_input = "#{user_input}#{next_char}"
            end
        rescue IO::WaitReadable
        end
        
        parse_management_input user_input
    end
end

def parse_management_input user_input
    clear_screen
    set_cursor 3,1
    case user_input
    when "n"
        puts "Create a VM"
        createvm
    when "d"
        print "Enter VM Name : "
        vmname = get_vm_name
        if ! vmname.empty?
            deletevm vmname
        else
            print 'Skipping action due to no VM Name entered'
        end
    when "t"
        print "Enter VM Name : "
        vmname = get_vm_name
        if ! vmname.empty?
            vm = get_properties vmname
            print_json vm if ! vm.nil?
        else
            print 'Skipping action due to no VM Name entered'
        end
    when "c"
        mainMenu
    else
        clear_screen 
        puts "Invalid or not implemented option"
    end
    managementSubMenu
end

def managementSubMenu
    actions = []
    set_cursor 1,1
    actions[0]= "New (#{RED}n#{NORMAL})" #7
    actions.push("Delete (#{RED}d#{NORMAL})") #10
    actions.push("Close Menu (#{RED}c#{NORMAL})") #14
    puts actions.join(' | ')
    get_management_input  
end

#State 
def get_state_input
    while user_input = $stdin.getch do
        begin
            while next_char = $stdin.read_nonblock(1) do
                user_input = "#{user_input}#{next_char}"
            end
        rescue IO::WaitReadable
        end
        
        parse_state_input user_input
    end
end

def parse_state_input user_input
    clear_screen
    set_cursor 3,1

    case user_input
    when "a"
        puts "Vagrant Global Status"
        printf "%4s\t%10s\t%15s\t%s\n"," id ","  state   ","    machine    ","     directory       "
        printf "%4s\t%10s\t%15s\t%s\n","----","----------","---------------","---------------------"
        vastatus = vstatus
        vastatus.each_with_index do | vmname, index|
            printf "%4s\t%10s\t%15s\t%s\n",index+1, vmname[3], vmname[1], vmname[4]
        end
    when "r"
        vmname = listrunning
        if vmname.size > 0
            vmname.each_with_index do | vm, index|
                puts "#{index+1})\t #{vm}"
            end
        else
            print "There are no virtual machines running" 
        end
    when "l"
        rows = $stdin.winsize[0]
        vmname = listvms
        if vmname.size > 0
            vmname.each_with_index do | vm, index|
            puts "#{index+1})\t #{vm}"
            end
        else
            print "There doesn't exist any virtual machines" 
        end
    when "c"
        mainMenu
    else
        clear_screen 
        puts "Invalid or not implemented option"
    end
    stateSubMenu
end

def stateSubMenu
    actions = []
    set_cursor 1, 1
    actions[0] = "Show VMs (#{RED}l#{NORMAL})" #12
    actions.push("Show Running Vms (#{RED}r#{NORMAL})") #20
    actions.push("Show Vagrant Status (#{RED}a#{NORMAL})") #23
    actions.push("Close Menu (#{RED}c#{NORMAL})") #14
    puts actions.join(' | ')
    get_state_input  
end

# Update
def get_update_input vmname
    while user_input = $stdin.getch do
        begin
            while next_char = $stdin.read_nonblock(1) do
                user_input = "#{user_input}#{next_char}"
            end
        rescue IO::WaitReadable
        end
        
        parse_update_input user_input,vmname
    end
end

def parse_update_input user_input,vmname
    clear_screen
    set_cursor 3,1
    case user_input
    when "a"
        adddiskvm vmname
    when "p"
        rezcpuvm vmname
    when "m"
        rezmemvm vmname
    when "c"
        mainMenu
    else
        clear_screen 
        puts "Invalid or not implemented option"
    end
    updateSubMenu vmname
end

def updateSubMenu vmname
    actions = []
    set_cursor 1,1
    actions[0] = "Add Disk (#{RED}a#{NORMAL})"
    actions.push("Resize Memory (#{RED}m#{NORMAL})")
    actions.push("CPU Count (#{RED}p#{NORMAL})")
    actions.push("Close Menu(#{RED}c#{NORMAL})")
    puts actions.join(' | ')
    get_update_input vmname
end

def set_cursor row=1, column=1
    print "\e[#{row};#{column}H"
end

def run
    read_config
    clear_screen
    mainMenu
end

def read_config
    $vb_home=nil
    $containers_dir=nil

    # Search for config files
    if File.exist? ("./vm.config")
        f = File.open("./vm.config", "r") 
        config_file = JSON.load f
        f.close
        $vb_home=config_file["vb_home"]
        $containers_dir=config_file["containers_dir"]
        $config_file_found=true
    elsif File.exist? ("~/vm.config")
        f = File.open("~/vm.config", "r") 
        config_file = JSON.load f
        f.close
        $vb_home=config_file["vb_home"]
        $containers_dir=config_file["containers_dir"]
        $config_file_found=true
    else
        $config_file_found=false
    end

    # Env Variables takes precedence over config file
    if ENV['VM_VB_HOME']
        $vb_home=ENV['VM_VB_HOME']
    end

    if ENV['VM_CONTAINERS']
        $containers_dir=ENV['VM_CONTAINERS']
    end

    # If no config or env variables found, then ask to provide locations
    if (!$config_file_found) || $vb_home.nil? || $containers_dir.nil?
        print "vm.config couldn't be found and env variables are not set\n"
        print "Do you want to provide VirtualBox and/or Vagrant file locations? [y|n] :"
        prop_input = gets.chomp

        if prop_input.empty?
            print "Exiting the program due to lack of positive answer\n"
            exit
        end

        case prop_input
        when "y"
            print "Enter or accept VB VM Machines path location [\"#$vb_home\"]: "
            user_input = gets.chomp
            if ! user_input.empty?
                $vb_home = user_input
            end
            print "Enter or accept Vagrant containers path location [\"#$containers_dir\"]: "
            user_input = gets.chomp
            if ! user_input.empty?
                $containers_dir = user_input
            end

            if $vb_home.empty? || $containers_dir.empty?
                print "Exiting the program due to either VirtualBox or Vagrant locations is empty\n"
                exit
            end

            if $vb_home.match? ('^~')
                $vb_home.gsub! '~', Dir.home
            end

            if $vb_home.match? (' ')
                $vb_home.gsub! ' ', '\ '
            end

            if $containers_dir.match? ('^~')
                $containers_dir.gsub! '~', Dir.home
            end

            if $containers_dir.match? (' ')
                $containers_dir.gsub! ' ', '\ '
            end

        when "n"
            print "We'll default to \"$HOME/VirtualBox VMs\" & \"$HOME/vagrant\"\n"            
            user_home = Dir.home
            if Dir.exist? ("#{user_home}/VirtualBox\ VMs")
                $vb_home="#{user_home}/VirtualBox\ VMs"
            end

            if Dir.exist? ("#{user_home}/vagrant")
                $containers_dir="#{user_home}/vagrant"
            else
                Dir.mkdir ("#{user_home}/vagrant")
                $containers_dir="#{user_home}/vagrant"
            end
            print "Enter any key to continue: "
            input = gets.chomp
        end
    end

    get_vm_state

    if ! Dir.exist? ("#$containers_dir/shared")
        puts "Creating #$containers_dir/shared directory..."
        Dir.mkdir ("#$containers_dir/shared")
    end
end

# Colors
NORMAL="\e[0m"
RED="\e[31;40m"
GREEN="\e[32;40m"
YELLOW="\e[33;40m"
BLUE="\e[34;40m"
WHITE="\e[37,40m"   

run